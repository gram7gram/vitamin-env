#!/bin/bash

mkdir -p vitamin-ss/web/imports/nomenclature
mkdir -p vitamin-ss/web/imports/remainders
mkdir -p vitamin-ss/web/imports/reserves

chmod 777 -R vitamin-ss/web/imports

# Create containers
docker-compose build

# Boot containers
docker-compose up -d

wget https://getcomposer.org/download/1.8.0/composer.phar

mv composer.phar vitamin-ss/composer.phar

# Install api dependencies
./php ./composer.phar install

# Install node dependencies

export NVM_DIR="${XDG_CONFIG_HOME/:-$HOME/.}nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

nvm install 6

nvm use 6

npm i

npm run build