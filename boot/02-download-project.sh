#!/bin/bash

cd /var/www

git clone https://bitbucket.org/gram7gram/vitamin-env --depth=1

cd vitamin-env

git clone https://bitbucket.org/itdoors-admin/vitamin-cs --depth=1 --branch release14.12

git clone https://bitbucket.org/itdoors-admin/vitamin-ss --depth=1 --branch release14.12

cp docker-compose.yml.dist docker-compose.yml

cp env/node/Dockerfile.dist env/node/Dockerfile

cp env/api/Dockerfile.dist env/api/Dockerfile

cp env/postgres/Dockerfile.dist env/postgres/Dockerfile

cp env/memcached/Dockerfile.dist env/memcached/Dockerfile

cp vitamin-ss/app/config/parameters.yml.dist vitamin-ss/app/config/parameters.yml

cp vitamin-ss/app/config/variables.yml.dist vitamin-ss/app/config/variables.yml

cp vitamin-cs/config/config.example.js vitamin-cs/config/config.js
