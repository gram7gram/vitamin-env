#!/bin/bash

DATABASE_NAME=vitamin

if [ -z "$DATABASE_NAME" ]; then echo "[-] Missing env variable DATABASE_NAME"; exit 1; fi;

CURRENT_DATE=$(date +\%Y-\%m-\%d_\%H_\%M_\%S)

echo "[+] Creating backup $CURRENT_DATE"

ID=$(/usr/local/bin/docker-compose ps -q db)

if [ ! -d /home/$USER/vitamin_backups ]; then mkdir /home/$USER/vitamin_backups; fi

/usr/bin/docker exec $ID pg_dump -Fc -d $DATABASE_NAME -U postgres > "/home/$USER/vitamin_backups/$DATABASE_NAME"_"$CURRENT_DATE".dump